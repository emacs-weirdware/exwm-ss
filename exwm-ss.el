;;; exwm-ss.el --- Manage screensaver activation     -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Ian Eure

;; Author: Ian Eure <ian@retrospec.tv>
;; Keywords: hardware, processes
;; Version: 0.1
;; Package-Requires: ((emacs "25.1") (exwm "0.23"))
;; URL: https://codeberg.org/emacs-weirdware/exwm-ss

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; EXWM-SS inhibits screensaver activation when it would be inconvenient.

;;; Code:

(require 'eieio)
(require 'exwm-workspace)

(defgroup exwm-ss nil
  "Inhibit screensaver activation."
  :group 'exwm)

(defcustom exwm-ss-controller-class 'exwm-ss-control-default
  "Which `EXWM-SS-CONTROL' implementation to use to control screensaver inhibition."
  :group 'exwm-ss
  :type 'symbol)

(defvar exwm-ss-controller nil
  "The current EXWM-SS controller object.")


 ;; Helper functions

(defun exwm-ss--buffers ()
  "Return a list of visible buffers in visible EXWM workspaces."
  (cl-loop for frame in exwm-workspace--list
           ;; If the frame is visible…
           when (frame-parameter frame 'exwm-active)
           ;; Get all its windows, and the buffers they're displaying.
           append (mapcar #'window-buffer (window-list frame))))

(defun exwm-ss--fullscreen? ()
  "Is the current buffer a fullscreen EXWM client?"
  (and (derived-mode-p 'exwm-mode) (exwm-layout--fullscreen-p) t))

(defmacro exwm-ss--with-home (&rest body)
  "Evaluate BODY with `default-directory' set to the user's home."
  `(let ((default-directory (getenv "HOME")))
      ,@body))

 ;; Base class: Control *how* screensaver/s are inhibited.

(defclass exwm-ss ()
  ((inhibit :initform nil
            :documentation "Whether to inhibit screensaver activation."))
  :abstract t
  :documentation "Base class for controlling screensavers.")

(cl-defmethod exwm-ss-set-inhibited! ((ss exwm-ss) inhibit)
  "Set whether screensaver activation should be inhibited."
  (setf (slot-value ss 'inhibit) inhibit))


 ;; EXWM-SS implementation: use xset(1) to control DPMS.

(defclass exwm-ss--dpms-command (exwm-ss)
  ()
  :documentation "EXWM-SS implementation which uses xset(1) to enable/disable DPMS.")

(cl-defmethod exwm-ss-set-inhibited! :after ((ss exwm-ss--dpms-command) inhibit)
  (let ((cmd (format "xset %sdpms" (if inhibit "-" "+"))))
    (exwm-ss--with-home (start-process-shell-command "exwm-ss-dpms" nil cmd))))


 ;; EXWM-SS implementation: Use xscreensaver-command to control xscreensaver

(defclass exwm-ss--xscreensaver (exwm-ss)
  ((timer :initform nil))
  :documentation "EXWM-SS implementation which prevents xscreensaver(1) from activating.")

(defun exwm-ss--xscreensaver-ping ()
  "Tell xscreensaver that there's been activity."
  (exwm-ss--with-home
   (start-process-shell-command "exwm-ss-xscreensaver" nil "xscreensaver-command -deactivate")))

(cl-defmethod exwm-ss-set-inhibited! :after ((ss exwm-ss--xscreensaver) inhibit)
  (with-slots (timer) ss
    (if inhibit
        (unless (and timer (timerp timer))
          (setf timer (run-at-time nil 60 #'exwm-ss--xscreensaver-ping)))

      (when timer
        (cancel-timer timer)
        (setf timer nil)))))


 ;; Base class: Control *when* screensaver/s are inhibited.

(defclass exwm-ss-control ()
  ((hook :initform nil)
   (hook-into :initform '(exwm-workspace-switch-hook
                          exwm-manage-finish-hook))
   (advise-after :initform '(exwm-layout-set-fullscreen
                             exwm-layout-unset-fullscreen
                             select-window)))
  :documentation "Composite EXWM-SS implementation which disables DPMS and xscreensaver.")

(cl-defmethod exwm-ss-control--workspace-changed ((ssc exwm-ss-control))
  "Set screensaver inhibition.

This is called in reaction to state changes (workspace switch,
etc), and controls whether the screensaver is inhibited or not."
  (let ((inh (exwm-ss-inhibited? ssc)))
    (exwm-ss-set-inhibited! ssc inh)))

(cl-defmethod initialize-instance :before ((ssc exwm-ss-control) &rest ignore)
  (unless (object-of-class-p ssc 'exwm-ss-control)
    (error "No `EXWM-SS' implementation in %s" (class-of ssc))))

(cl-defmethod initialize-instance :after ((ssc exwm-ss-control) &rest ignore)
  (declare (ignore ignore))
  ;; This removes duplication of the hook function across adding and
  ;; removing hooks.
  (with-slots (hook) ssc
    (setf hook (lambda (&rest ignore)
                 (declare (ignore ignore))
                 (exwm-ss-control--workspace-changed ssc)))))

(cl-defmethod exwm-ss-control--setup ((ssc exwm-ss-control))
  "Begin controlling screensaver."
  (with-slots (hook hook-into advise-after) ssc
    (dolist (h hook-into)
      (add-hook h hook))

    (dolist (fun advise-after)
      (add-function :after (symbol-function fun) hook
                    '(name exwm-ss-hook))))
  ;; Fixup state, EXWM-SS may have been enabled when the screensaver
  ;; should be inhibited.
  (exwm-ss-control--workspace-changed ssc))

(cl-defmethod exwm-ss-control--teardown ((ssc exwm-ss-control))
  "Stop controlling screensaver."
  (with-slots (hook hook-into advise-after) ssc
    (dolist (h hook-into)
      (remove-hook h hook))

    (dolist (fun advise-after)
      (remove-function (symbol-function fun) 'exwm-ss-hook))))

(cl-defgeneric exwm-ss-inhibited? (ssc)
  "Should the screensaver activation be inhibited?")


 ;; EXWM-SS-CONTROL implementation: Base

(defclass exwm-ss-control-base (exwm-ss-control)
  ()
  :documentation "Default implementation of `EXWM-SS-CONTROL'.

This class includes the default logic for when the screensaver
should be inhibited, but doesn't specify how inhibition is
accomplished.")

(cl-defmethod exwm-ss-special? ((ssc exwm-ss-control-base))
  "Is the current buffer special?

Special buffers inhibit the screensaver when they're visible."
  (or (string= exwm-class-name "Jellyfin Media Player")
      (string= exwm-class-name "mpv")
      (string= exwm-class-name "vlc")))

(cl-defmethod exwm-ss-inhibits? ((ssc exwm-ss-control-base) buf)
  "Does BUF inhibit screensaver activation?

This is called after the buffer list has been filtered down to
ones being displayed in visible EXWM frames, so no frame
visibilty is checked in this function."
  (with-current-buffer buf
    (and
     ;; If the buffer is fullscreen or special…
     (or (exwm-ss--fullscreen?) (exwm-ss-special? ssc))
     ;; …then inhibit
     t)))

(cl-defmethod exwm-ss-inhibited? ((ssc exwm-ss-control-base))
  "Should the screensaver activation be inhibited?"
  (cl-loop for buf in (exwm-ss--buffers)
           when (exwm-ss-inhibits? ssc buf)
           return t))

 ;; Debounce

(defclass exwm-ss-control-debounce ()
  ((debounce :initarg :debounce :initform 3
             :documentation "Debounce delay.")
   (timer :initform nil)))

(cl-defmethod exwm-ss-control--workspace-changed ((ssc exwm-ss-control-debounce))
  (with-slots (debounce timer) ssc
    (ignore-errors (cancel-timer timer))
    (run-with-timer debounce nil
                    (lambda ()
                      (cl-call-next-method ssc)
                      (ignore-errors (cancel-timer timer))))))

 ;; EXWM-SS-CONTROL implementation: Default

(defclass exwm-ss-control-default (exwm-ss-control-base exwm-ss--dpms-command exwm-ss--xscreensaver)
  ()
  :documentation "Default `EXWM-SS-CONTROL' implementation.

This class uses the default logic, and DPMS + xscreensaver inhibition
mechanisms.")


 ;; Minor mode

;;;###autoload
(define-minor-mode exwm-ss-mode
  "Minor mode to disable the screensaver when one or more X clients are fullscreen."
  :global t
  (if exwm-ss-mode
      (exwm-ss-control--setup
       (or exwm-ss-controller
           (setq exwm-ss-controller (make-instance exwm-ss-controller-class))))
    (progn
      (exwm-ss-set-inhibited! exwm-ss-controller nil)) ; Reenable screensaver
      (exwm-ss-control--teardown exwm-ss-controller)
      (setq exwm-ss-controller nil)))

(defun exwm-ss-status ()
  (interactive)
  (message
   (cond
    ((and exwm-ss-mode exwm-ss-controller (slot-value exwm-ss-controller 'inhibit))
     "Active, inhibiting screensaver.")
    ((and exwm-ss-mode exwm-ss-controller (not (slot-value exwm-ss-controller 'inhibit)))
     "Active, not inhibiting screensaver.")
    (t "Inactive."))))

(provide 'exwm-ss)
;;; exwm-ss.el ends here
